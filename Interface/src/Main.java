/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 *
 * "Jawab disini..."
 * Pada kondisi multiple inheritance, interface lebih cocok digunakan daripada abstrak class,
 * hal ini dikarenakan sebuah class dapat mengimplements banyak interface tapi untuk extends hanya dapat dilakukan pada satu abstrak class saja.
 * Dalam kasus kalkulator kita pasti akan melayani berbagai macam operasi matematika seperti pertambahan, pengurangan, perkalian, pembagian, dan lain sebagainya.
 * Oleh karena itu, untuk melayani banyaknya operasi yang ada, penerapan interface 'dengan memberikan perlakuan sama
 * kepada semua objek dari berbagai kelas' ternilai lebih cocok untuk sisi fleksibiitas program.
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}