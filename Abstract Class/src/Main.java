/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 *
 * "Jawab disini..."
 * Pada kasus kalkulator, jika terdapat kebutuhan untuk memberikan implementasi default,
 * maka penggunaan abstrak class lebih cocok dalam penyusunan kode programnya.
 * Misal saja sebagai contoh jika kita memiliki abstrak class Kalkulator dengan
 * implementasi default untuk metode validasiOperan(), maka subclass-subclass yang meng-extends Kalkulator,
 * seperti Pertambahan dan Pengurangan akan mewarisi implementasi default validasiOperan() secara otomatis.
 * Artinya, jika kita tidak perlu pengubahan pada implementasi default,
 * maka kita tidak perlu menuliskan ulang metode validasiOperan() di subclass-subclass tersebut.
 */

 import java.util.*;

 public class Main {
     public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);
 
         System.out.print("Masukan operan 1: ");
         double operand1 = scanner.nextDouble();
 
         System.out.print("Masukan operan 2: ");
         double operand2 = scanner.nextDouble();
 
         System.out.print("Masukan operator (+ atau -): ");
         String operator = scanner.next();
         scanner.close();
 
         Kalkulator kalkulator;
         if (operator.equals("+")) {
             kalkulator = new Pertambahan();
         } else if (operator.equals("-")) {
             kalkulator = new Pengurangan();
         } else {
             System.out.println("Operator tidak valid!");
             return;
         }
 
         kalkulator.setOperan(operand1, operand2);
         double result = kalkulator.hitung();
         System.out.println("Hasil: " + result);
     }
 }